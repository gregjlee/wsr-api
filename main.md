FORMAT: 1A

# WSR API
WSR API is a **pastes service** similar to [GitHub's Gist](http://gist.github.com).

## Authentication
Currently the WSR API does not provide authenticated access.

## Media Types
Where applicable this API uses the [HAL+JSON](https://github.com/mikekelly/hal_specification/blob/master/hal_specification.md) media-type to represent resources states and affordances.

Requests with a message-body are using plain JSON to set or update resource states.

## Error States
The common [HTTP Response Status Codes](https://github.com/for-GET/know-your-http-well/blob/master/status-codes.md) are used.

## API Blueprint
+ [Previous: Named Resource and Actions](https://app.apiary.io/wsr)
+ [This: Raw API Blueprint](https://raw.github.com/apiaryio/api-blueprint/master/examples/4.%20Grouping%20Resources.md)
+ [Next: Responses](5.%20Responses.md)

# Gist WSR API Root [/api/]
Gist WSR API entry point.

This resource does not have any attributes. Instead it offers the initial API affordances in the form of the HTTP Link header and 
HAL links.
## Retrieve the Entry Point [GET]
+ Response 200 (application/hal+json)
    + Headers
    
            Link: <http:/www.westsiderentals.com/api/>"

    + Body

            {
                
            }

# Group Listing
Listing-related resources of *WSR API*.

## MapSearch [/getMapLight.cfc]
Get partial listing data for a map area


+ Parameters
    + coordinates (string) ... Coordinates of polygon to search
    + priceLow (integer) ... lowest price allowed, 0 for any
    + priceHigh (integer) ... highest price allowed, 0 for any
    + leaseTypeList (string) ... list of lease type id's seperated by commas, 0 for any
    + propTypeList (string) ... list of property type id's seperated by commas, 0 for any
    + bedTypeList (string) ... list of bed type id's seperated by commas, 0 for any
    + petsList (string) ... list of pet type id's seperated by commas, 0 for any
    + furnList (string) ... list of furniture type id's seperated by commas, 0 for any

+ Model (application/hal+json)

    HAL+JSON representation of Gist Resource. In addition to representing its state in the JSON form it offers affordances in the form of the HTTP Link header and HAL links.


    + Body

            {
                 (
                    {
                    "BATHROOM_TYPE_ID" = 1;
                    "BEDROOM_TYPE_ID" = 8;
                    LAT = "34.0204492164";
                    "LISTING_ID" = 25767;
                    LON = "-118.486535472";
                    "ORIG_PHOTO_URL" = "landlords/photos/201003/03112010170010_1517.bmp";
                    "PRICE_FROM" = "1275.0";
                    "PROPERTY_ID" = 68260;
                    }
                )

            }

### getMap method [GET]
+ Request (application/json)

        {
            coordinates = POLYGON((-118.526809 33.974509,-118.457109 33.974509,-118.457109 34.073436,-118.526809 34.073436,-118.526809 33.974509))
            priceLow = 1000
            priceHigh = 2000
            leaseTypeList = 0
            propTypeList = 12
            bedTypeList = 4,17,3,5,16,1,6,7,2
            bathTypeList = 0
            petsList = 0
            furnList = 0
        }

+ Response 200
    
    [MapSearch][]

# Group Member
Member-related resources of *WSR API*.