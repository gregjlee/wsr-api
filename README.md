[Home] |
[Listing] |
[Member] |
[Favorites] |
[Saved Searches] |
[Listing Alerts] |
[Membership Purchases] |
[Rental Application] |

[Home]:https://bitbucket.org/gregjlee/wsr-api/wiki/Home
[Listing]:https://bitbucket.org/gregjlee/wsr-api/wiki/listing
[Member]:https://bitbucket.org/gregjlee/wsr-api/wiki/Member
[Favorites]:https://bitbucket.org/gregjlee/wsr-api/wiki/Favorites
[Saved Searches]:https://bitbucket.org/gregjlee/wsr-api/wiki/SavedSearch
[Listing Alerts]:https://bitbucket.org/gregjlee/wsr-api/wiki/ListingAlerts
[Membership Purchases]:https://bitbucket.org/gregjlee/wsr-api/wiki/MembershipPurchases
[Rental Application]:https://bitbucket.org/gregjlee/wsr-api/wiki/RentalApplication


#Welcome
BaseURL: http://www.westsiderentals.com/api/  
**Secret Shake - Add as the last parameter unless noted**
#API Call Example
An example of using the API with the map search call (getMapLight.cfc).  Although most calls have the secret Shake as the last parameter, GetMap requires it to be put first.
In this section, the full XML request and response will be shown.  **Normally, the requests and responses will be shown without the xml tags**
###Map Search Request
+ Full XML request
```sh
<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns="http://api">
        <soap:Body>
            <getMap>
                <secretShake>secretShake</secretShake>
                <coordinates>POLYGON((-118.526758 33.974520,-118.457058 33.974520,-118.457058 34.073447,-118.526758 34.073447,-118.526758 33.974520))</coordinates>
                <priceLow>1000</priceLow>
                <priceHigh>2000</priceHigh>
                <leaseTypeList>0</leaseTypeList>
                <propTypeList>12</propTypeList>
                <bedTypeList>5,4,2,3,1,6,17,7,16</bedTypeList>
                <bathTypeList>0</bathTypeList>
                <petsList>0</petsList>
                <furnList>0</furnList>
            </getMap>
    </soap:Body>
</soap:Envelope>
```

The parameters in the message are enclosed by a soap envelope with the name of the method, in this case the method is “getMap”

###Map Search Response

+ Full XML response  
```sh
<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<soapenv:Body>
    <ns1:getMapResponse xmlns:ns1="http://api" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
        <getMapReturn xmlns:ns2="http://rpc.xml.coldfusion" xsi:type="ns2:QueryBean">
            <columnList xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" soapenc:arrayType="xsd:string[8]" xsi:type="soapenc:Array">
                <columnList xsi:type="xsd:string">PROPERTY_ID</columnList>
                <columnList xsi:type="xsd:string">LAT</columnList>
                <columnList xsi:type="xsd:string">LON</columnList>
                <columnList xsi:type="xsd:string">PRICE_FROM</columnList>
                <columnList xsi:type="xsd:string">LISTING_ID</columnList>
                <columnList xsi:type="xsd:string">ORIG_PHOTO_URL</columnList>
                <columnList xsi:type="xsd:string">BATHROOM_TYPE_ID</columnList>
                <columnList xsi:type="xsd:string">BEDROOM_TYPE_ID</columnList>
            </columnList>
            <data xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" soapenc:arrayType="xsd:anyType[][5]" xsi:type="soapenc:Array">
                <data soapenc:arrayType="xsd:anyType[8]" xsi:type="soapenc:Array">
                    <data xsi:type="soapenc:int">589746</data>
                    <data xsi:type="soapenc:double">34.02835</data>
                    <data xsi:type="soapenc:double">-118.49677</data>
                    <data xsi:type="soapenc:double">1875.0</data>
                    <data xsi:type="soapenc:int">820333</data>
                    <data xsi:type="soapenc:string">landlords/photos/201005/05162010140613_apt5eatingareaWinCE.jpg</data>
                    <data xsi:type="soapenc:int">1</data>
                    <data xsi:type="soapenc:int">1</data>
                </data>
                <data soapenc:arrayType="xsd:anyType[8]" xsi:type="soapenc:Array">
                    <data xsi:type="soapenc:int">856295</data>
                    <data xsi:type="soapenc:double">34.022673</data>
                    <data xsi:type="soapenc:double">-118.489573</data>
                    <data xsi:type="soapenc:double">1695.0</data>
                    <data xsi:type="soapenc:int">1113898</data>
                    <data xsi:type="soapenc:string">landlords/photos/201405/05072014070410_front.jpg</data>
                    <data xsi:type="soapenc:int">1</data>
                    <data xsi:type="soapenc:int">1</data>
                </data>
            </data>
        </getMapReturn>
    </ns1:getMapResponse>
</soapenv:Body>
</soapenv:Envelope>
```
For a response with multiple results, the column names are included before the array of results  



    