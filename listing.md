# WSR API
####[Main]
####[Listing]

[Main]:https://bitbucket.org/gregjlee/wsr-api/src/bff1794b3f6ea484a7e3cf50af4b0253ec36fa23/README.md?at=master
[Listing]:https://bitbucket.org/gregjlee/wsr-api/src/e5c1f44def5a58fe09532b8155f62e8abb66ff69/listing.md?at=master


#Listing 
Listing-related resources of *WSR API*.

## MapSearch [getMapLight.cfc]
Get partial listing data for a map area

### getMap method
+ SecretShake - put as first parameter
+ Parameters Notes
    + coordinates - coordinates of polygon to search
    + 0 for any
+ Request 
```sh
coordinates = POLYGON((-118.526809 33.974509,-118.457109 33.974509,-118.457109 34.073436,-118.526809 34.073436,-118.526809 33.974509))
priceLow = 1000
priceHigh = 2000
leaseTypeList = 0
propTypeList = 12
bedTypeList = 4,17,3,5,16,1,6,7,2
bathTypeList = 0
petsList = 0
furnList = 0
```

+ Response 
```sh
 (
    {
    "BATHROOM_TYPE_ID" = 1;
    "BEDROOM_TYPE_ID" = 8;
    LAT = "34.0204492164";
    "LISTING_ID" = 25767;
    LON = "-118.486535472";
    "ORIG_PHOTO_URL" = "landlords/photos/201003/03112010170010_1517.bmp";
    "PRICE_FROM" = "1275.0";
    "PROPERTY_ID" = 68260;
    }
)
```

## Area Search [getAreaSearchLight.cfc]
Get partial listing datat for selected areas

### getAreaSearchLight method
+ SecretShake - put as first parameter
+ Parameters notes
    * 0 for any
+ Request 
```sh
areaIds = 87,65
priceLow = 1000
priceHigh = 2000
propTypeList = 12
leaseTypeList = 0
bedTypeList = 4,17,3,5,16,1,6,7,2
bathTypeList = 0
petsList = 0
furnList = 0
```

+ Response 
```sh
(
    {
        "BATHROOM_TYPE_ID" = 1;
        "BEDROOM_TYPE_ID" = 1;
        LAT = "33.996964";
        "LISTING_ID" = 1114743;
        LON = "-118.422757";
        "ORIG_PHOTO_URL" = "";
        "PRICE_FROM" = "1050.0";
    }
)
```

## Get Listing [getListing.cfc]
Get the full listing data

### getListing method

+ Parameter Notes
    + listing_id - listing identifier
+ Request
```sh
listing_id = 123456
```
+ Response
```sh
ACCESSIBLE = false;
"AIR_CONDITIONER" = false;
"ALT_ADDRESS" = "10st near Montana";
"APP_FEE_REQUIRED" = false;
"AREA_ID" = 207;
"AREA_NAME" = "Santa Monica";
"ASSOCIATION_FEES" = false;
"AVAIL_DATE" = "2014-06-03T07:00:00.000Z";
"AVAIL_DATE_TEXT" = "";
BALCONY = false;
"BATHROOM_TYPE_ID" = 1;
"BEDROOM_TYPE_ID" = 1;
CABLE = false;
CALENDAR = "";
CALENDARTYPE = "";
"CC_PROP_VERIFIED" = 1209526;
"CENTRAL_AIR" = false;
"CENTRAL_HEAT" = true;
"CONFIRMED_BY" = "Landlord on web";
"CONTACT_ALT_PHONE" = "";
"CONTACT_ALT_PHONE_EXT" = "";
"CONTACT_EMAIL" = "";
"CONTACT_FAX" = "";
"CONTACT_NAME" = nancy;
"CONTACT_PHONE" = "(424) 245-6950";
"CONTACT_PHONE_EXT" = "";
"CONTROLLED_ACCESS" = false;
"CREDIT_CHECK_COMPLIANCE" = true;
"DATE_CONFIRMED" = "2014-05-07T04:18:00.000Z";
"DATE_FEATEXP" = "";
DEPOSIT = 2500;
DESCRIPTION = "upper N of Wilshire Prime Santa Monica hardwood floor Bright";
DISHWASHER = true;
DONTSHOWUNITNO = true;
ELECTRICITY = false;
ELEVATOR = false;
EXCLUSIVE = false;
FEATURED = false;
FIREPLACE = false;
"FLOORING_TYPE_ID" = 3;
"FURNISHED_TYPE_ID" = 1;
GARDENER = true;
GAS = false;
HASADVANCEDFLOORPLAN = "";
HASFLOORPLAN = false;
HASPHOTO = true;
HASVIDEO = false;
HASVIRTTOUR = false;
"HOT_WATER" = true;
ISPHOTOPLUS = false;
ISWSRMANG = false;
"KEY_COUNT" = 0;
"KEY_OFFICE_ID" = 0;
"LANDLORD_ID" = 1209526;
"LANDLORD_TYPE_ID" = 1;
"LAST_PRICE_REDUCE" = "1900-01-01T08:00:00.000Z";
LAT = "34.02835";
LAUNDRY = true;
"LEASE_TYPE_ID" = 1;
"LISTING_ID" = 820333;
"LISTING_TYPE_ID" = 2;
"LIST_DATE" = "2014-05-02T15:26:00.000Z";
LON = "-118.49677";
"MAID_SERVICE" = false;
MICROWAVE = false;
"OPEN_HOUSE" = "";
"ORIG_PHOTO_URL" = "landlords/photos/201005/05162010140613_apt5eatingareaWinCE.jpg";
OTHERAMENITIES = "Bright and sunny, nice quiet bldg, 1 block to Montana shopping, entertainment & fine dining, close to the beach, Hardwood floor, stove,dish washer walk-in mirror closet, smooth ceiling with recess lighting.\n\n\nRent Control fee $28";
"PARKING_QTY" = 1;
"PARKING_TYPE_ID" = 1;
PATIO = false;
"PET_TYPE_ID" = 1;
POOL = false;
"POOL_ON_SITE" = "";
"POOL_SERVICE" = false;
"PRICE_FROM" = "1875.0";
"PRICE_TO" = "";
"PROPERTY_ID" = 589746;
"PROPERTY_TYPE_ID" = 12;
"QUIET_NHOOD" = true;
REFRIGERATOR = false;
"REGION_ID" = 1;
REMOVED = false;
RENTED = false;
REPLACED = 0;
SPECIALOPENHOUSE = "";
SQUAREFOOTAGE = "aprox 750";
STOVE = true;
"STREET_NAME" = "Idaho Ave";
"STREET_NUMBER" = 1025;
"STREET_TYPE_ID" = 1;
"TOTAL_UNITS" = 11;
TRASH = true;
"UNIT_NO" = "";
"UNIT_TYPE_ID" = 3;
"USE_ALT_ADDRESS" = true;
UTILITIES = false;
"UTILS_PARTIAL" = false;
"VT_URL" = "";
WATER = true;
WD = false;
"WD_HOOKUPS" = false;
YARD = false;
ZIPCODE = 90403;
```

## Listing Photos [getPhotos.cfc]
Get the photo urls for a listing
### getPhotos method 
+ bug - returns wrong base url, switch "www.westsiderentals.com/" with "static.westsiderentals.com/"
+ Request 
```sh
listing_id =233445
```

+ Response
```sh
(
    {
    FILEPATH = "http://www.westsiderentals.com/photos/landlords/photos/201004/04032010105130_014.JPG";
    }
)
```
    